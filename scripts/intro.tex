\chapter{Introduction} 

\section{Overview}

In this chapter an introduction to the theoretical bases of biology and bioinformatics are given. The chapter starts with a description of important molecular biology aspects and explains the need for specific analysis algorithms to produce accurate insights into biological processes. Further, it focuses on transcriptome investigation, defining accomplished blocks of this work and current research procedures. A detailed explanation of high-throughput sequencing technology and RNA-seq analysis procedure are given. Finally, an overview of active research projects and explanation of further chapters are provided.  

\section{Molecular biology and bioinformatics}

\subsection{Central dogma of molecular biology}

Biology is a sophisticated science that ranges from the chemical construction of molecules that create biological processes to investigation of complex activity of all living creatures. Organisms have large differences in classification: bacteria, fungi, plants, animals. However, each organism starts with the development from a cell. For example, the human body includes several hundreds of distinct cell types and in total it consists of approximately \(3.72 * 10^{13}\) cells \cite{bianconi2013estimation}. Even though there are many different types of cells with various activity in the human body, cells are packed in blocks of the same type and work together to perform certain functions of the organism. Detailed investigation of cell structure and activity is a main aspect of molecular biology. 

% More about cells: http://www.brighthubeducation.com/science-homework-help/106593-types-of-cells-in-the-human-body/
 
There are special chemical rules that control each cell life cycle. The process starts from deoxyribonucleic acid (DNA), which can be detected in all cells of all organisms. DNA is the "holder" of all information of cell activity. From the DNA the activity instructions are transfered with ribonucleic acid (RNA). Finally RNAs are converted to proteins.  Proteins along with several types of RNAs perform all cellular functions. The theory of activity between DNA, RNA and proteins was first introduced by Francis Crick \cite{crick1958protein} and called \textit{central dogma of molecular biology}. After the initiation of the dogma, further research works allowed to qualify the cell system in more detail. 

\begin{figure}[t] 
	\begin{center}
		\caption[Central dogma of molecular biology]{Central dogma of molecular biology } 
		\label{fig:central_dogma}
		\includegraphics[width=1\textwidth]{figures/main_figure.pdf} 
	\end{center}
\end{figure}

Main processes of cell activity are the following:
\begin{enumerate}
	\item DNA maintains all the cell information via the \textbf{replication process}, where DNA sequence is copied to produce a nearly identical molecule  
	\item \textbf{Transcription} of specific DNA fragments to RNA is a temporary copy of DNA used to create proteins and also play distinct functional roles in translational apparatus
	\item RNAs are \textbf{translated} to proteins, which  perform all structural functions and play regulatory roles. 
	\item \textbf{Reverse transcription} of RNA is a template for the synthesis of DNA applied for pseudogene replication and other types of transposition.    
\end{enumerate} 

The first publication that reported identification of DNA structure and activity \cite{watson1953molecular} led to an acceleration of research progress in all aspects of the cell system and its activity in connection to organism. The research produced a detailed understanding of various biological processes beginning with DNA. The organization of DNA in cells is currently considered at specific structural levels: nucleotides, sequences, chromosomes and genome. A nucleotide is composed of a monosaccharide sugar called deoxyribose, a 5'phosphate group and a specific nucleobase:  cytosine , guanine , adenine  or thymine. Each nucleotide (also called base) is connected to a specific partner,forming a double strand: adenine to thymine, cytosine to guanine. The DNA information is collected in a sequence of nucleotides. Chromosomes are physical blocks, containing DNA. The genome is a full collection of all DNA in a cell.

In RNA molecules the thymine nucleobase is replaced with uracil and an oxygen atom is added to the sugar component during the transcription process. These two small differences have a major impact on the biological role of the molecule. RNA is more chemically active and it usually consists of a single strand of DNA. There are two types of its initial genomic location: forward or reverse strand. An easy way to maintain information about DNA or RNA is with a string of four symbols:  \[ \{ A,C,G,T \} \] with left-to-right orientation corresponding to 5' to 3' polarity in case of DNA and correct strand of transcription in case of RNA. Using this technique it is possible to keep information about fragments of DNA chromosomes or expressed RNA sequences to perform further investigations.

%TODO: small block about proteins?

From the start molecular biology had a requirement for detailed analysis of cell processes and functions. The first experiments that allowed detection of sequences of DNA and RNA required approaches to understand the translation of DNA to proteins, discovery of similarity between sequences, and much more. To accomplish these tasks, special computational algorithms were required. Additionally, the quantity of novel scientific results was growing and special data collection systems were important. Because of these reasons bioinformatics started its progress. Good examples of the initial computational approaches were conversion of DNA sequences to protein or the Smith-Waterman algorithm for detection of similar DNA/RNA sequences \cite{smith1981identification}.

\subsection{Intricacy of transcriptome}

RNA forms a connecting block between DNA and protein in a cell life, however it plays a huge role in the data transfer to perform functional processes of a cell. DNA segments that  contain information about cell functions - \emph{genes} - are expressed as RNA molecules   -  \emph{transcripts}. Additionally there are special RNAs that participate in the translational apparatus and have specific functions. The \textit{transcriptome} is a complete set of transcripts in a cell or tissue. The detailed spectrum of transcriptome functionality regulates the selection of expressed genes in different cell types and changes the activity according to external conditions. \cite{maniatis2002extensive}. Moreover, transcriptome functions are important for health and incorrect RNA process might lead to disease \cite{mitelman2007impact}.

There is a number of RNA types with different functionality forming the transcriptome. Best known are the elements participating in the protein synthesis:
\begin{itemize}
\item Messenger RNA (mRNA) 

mRNA is transcribed from genes and used to construct a protein. It carries genetic information specific for the activity of a cell type. Even though the transcripts are crucial, they make up only 5\%of the total transcriptome. During the transcription process a \textit{precursor mRNA} molecule is synthesized and a poly-A tail of about 200 bp is added to the 3' end. It is  worth noting that mRNA in higher eukaryotes  (including Homo sapiens) contains non-coding segments of mRNA called \textit{introns}. These blocks are extracted and only the remaining segments - \textit{exons} - are applied to construct the protein. The whole process is called splicing (Figure \ref{fig:mrna_process}). Importantly, genes might have several combinations of selected  exons (so called alternative splicing) and this leads to several different isoforms of a gene. For example, in human cells there are around six transcripts per protein-coding gene on average \cite{encode2012integrated}.  

\begin{figure} 
	\begin{center}
		\caption[Transcription process in high eukaryotes]{Transcription process in high eukaryotes } 
		\label{fig:mrna_process}
		\includegraphics[width=1\textwidth]{figures/mrna_process.pdf} 
	\end{center}
\end{figure}

\item Ribosomal RNA (rRNA)
  
rRNA is located in the cytoplasm of a cell, where ribosomes are found. rRNA directs the translation of mRNA into proteins. This is the largest part of the transcriptome and essential for protein synthesis in all living organisms.

\item Transfer RNA (tRNA)

tRNA is located in the cellular cytoplasm and also involved in protein synthesis. tRNA brings or transfers amino acids to the ribosome that correspond to each of the three-nucleotide codon of rRNA. The amino acids then can be joined together and processed to make polypeptides and proteins.
\end{itemize}

Additionally, a number of specific RNA types are present in eukaryotes. Even though these RNAs form less than 1\% of total RNA, they might perform important cell regulatory operations. Several distinct RNAs such as small nuclear RNA together with ribonuclease  participate in post-transcription modification and processing  of pre-mRNA \cite{mamatis1987role}. Long non-coding RNAs regulate gene transcription \cite{rinn2012genome}. Small interfering RNA and microRNA  are responsible for regulation of gene expression \cite{ambros2004functions}. Antisense RNA block mRNA translation and expression \cite{brantl2007regulatory}.  

%TODO: about microRNA: http://bib.oxfordjournals.org/content/16/5/780.short?rss=1	
%TODO: add circular RNA

\subsection{Analysis of transcriptome} 

The first experiments to analyze the transcriptome were performed using methods similar to DNA analysis. The extraction of RNA from a cell followed by amplification and conversion of RNA to complementary DNA (cDNA) allowed the application of a standard method called reverse transcription polymerase chain reaction (RT-PCR), that has become one of the fundamental technologies in molecular biology \cite{mullis1992specific}. Such experiments provide confirmation of the presence of a certain expressed transcript or other RNA. The analysis can be performed only if a specific primer pair with sequence blocks appropriate to the target are available. The next important step was the development of the quantitative RT-PCR technique \cite{becker1989absolute}, which allows measurement of the gene expression level with high sensitivity. The limitation of this method lay in a restricted number of genes to test and labor-intensive work. 
%It is also important to notice that certain steps in PCR analysis require to adapt correct primer pairs to certain sequence. This aspect can be solved correctly only with computational process and bioinformatics applications have become standard for such experiments. !REFprimer3!   

A big advance in DNA and RNA analysis was Sanger sequencing \cite{sanger1977dna}. This method is based on the selective incorporation of chain-terminating nucleotides by the enzyme DNA polymerase during DNA replication. To apply this method for RNA, it also has to be converted to cDNA. Generally, this approach allowed correct detection of a detailed sequence of analyzed transcript. For a long time this technology was regarded as the "gold" standard due to its low error rate.

A certain improvement in transcriptome analysis was the microarray approach \cite{harrington2000monitoring}.  This technique applies a collection of microscopic DNA spots attached to a solid surface and allowed measurement of the expression levels of thousands of genes simultaneously. Due to the speed, RNA microarray analysis has become an essential component of biology and biomedical research.

Even though a number of RNA experimental analysis methods were available, certain limitations were hindering the detailed investigation of transcriptome structure and activity, which required novel approaches. 


\section{High-throughput sequencing}

\subsection{Initial approaches} 

A huge step forward in transcriptome analysis was taken when Next Generation Sequencing (NGS) technologies entered the field of molecular biology. NGS, also called high-throughput sequencing (HTS) technology, outputs sequences of millions of DNA strands in parallel, providing substantially higher throughput than the Sanger approach in a short time period and minimizing the need for the fragment cloning methods.  The first NGS approach called pyrosequencing allowed the creation of \textit{reads}: fragments of DNA providing whole genome segments of a certain size \cite{ronaghi1998sequencing}. Initial NGS experiments also adapted the cDNA approach to detect the transcriptome activity from a cell \cite{morin2008profiling}. 

The development of HTS quickly went further. Technology was improving in the increased read length and process quality. At the same time, costs were falling. Currently there are three benchtop companies that provide HTS instruments: Illumina (www.illumina.com), Roche 454 (www.454.com) and Life Technologies (www. lifetechnologies.com). Notably, Illumina has presently the highest usage percentage - around 75\% of sequencing applications. Importantly, the generated sequencing data has a complex structure and large size. Therefore, to achieve correct results from the data, detailed algorithmic approaches are required. Because of this, the importance of bioinformatics in molecular biology has increased significantly. 

%TODO: detailed description of sequencing with a figure?

\subsection{RNA sequencing}

The initial RNA sequencing process was introduced quite fast \cite{mortazavi2008mapping, nagalakshmi2008transcriptional}. Once again, the main change in the sequencing method was extraction of RNA from the cell and conversion to cDNA. The typical mRNA sequencing approach is demonstrated in Figure \ref{fig:rna_seq_process}. The process starts with extraction of total RNA from a cell. Then, either polyA enrichment or reduction of ribosomal RNA is applied to increase the proportion of mRNA and other elements of transcriptome. Next, the conversion of processed RNA to double-strand cDNA is performed applying random primer hybridization. The generated cDNAs are broken into fragments of a certain size (typically 200-500 bp). These fragments are marked by adapter ligation from one or both ends, resulting in single-end reads or in paired-end reads. The sequencing process is performed after PCR amplification. A single Illumina sequencing run can produce up to hundred of millions of reads with a size of 100 bp. Importantly, each step of the sequencing procedure might introduce biases and errors \cite{van2014library}.   

\begin{figure} 
	\begin{center}
		\caption[The RNA-sequencing process structure]{RNA-sequencing process. 1) Total RNA is extracted from cells and separated from rRNA 2) Transcripts are converted to cDNA, multiplied and broken into fragments 3) Reads are generated from cDNA fragment ends 4) Analysis of reads is performed through alignment to reference (a) or assembly (b)  } 
		\label{fig:rna_seq_process}
		\includegraphics[width=1\textwidth]{figures/rna_seq_process.pdf} 
	\end{center}
\end{figure}

RNA-sequencing has significant advantages in comparison to previous technologies. For example, RNA-seq outperforms microarray approaches with higher accuracy and detection rate in expression analysis \cite{fu2009estimating}. The main goals of RNA-seq application are currently:

\begin{enumerate}
	
	\item Quantification of gene expression of each transcript based on the cell type
	
	\item Comparison of expression levels of cells between various biological conditions
	
	\item Annotation of all expressed genes including their splice junctions (breaks between introns)
		
\end{enumerate}

Additionally during past years RNA-seq introduced a number of specific issues that enabled discovery of novel events in the transcriptome. However, RNA-seq data analysis requires a lot of data processing along with specialized algorithms solving these tasks correctly and efficiently. There are typical analysis pipelines applied frequently to solve these tasks, even though certain questions still remain unanswered.     

\section{Transcriptome analysis using RNA-sequencing}

\subsection{Data processing overview}

 Figure \ref{fig:rnaseq_data_analysis} illustrates the process of RNA-sequencing data analysis, including typical steps such as initial quality control of reads in FASTQ format, further processing techniques (alignment and assembly), and finally gene expression analysis and comparison. Additionally, certain novel approaches of RNA-seq application are usually applied after performing alignment or assembly. Detailed explanations of each step are provided further. 

\begin{figure} 
	\begin{center}
		\caption[RNA-sequencing data analysis overview]{RNA-sequencing data analysis overview} 
		\label{fig:rnaseq_data_analysis}
		\includegraphics[width=0.8\textwidth]{figures/rna_seq_data_analysis.pdf} 
	\end{center}
\end{figure}

\subsection{Quality control of sequencing data}

High-throughput sequencing is a powerful technology, however it has a complex structure and processing steps, therefore a careful design of data analysis is required. Moreover, due to the multilevel structure of the full experiment a number of problematic issues might occur. The process can suffer from inconsistent sample and library preparation, specific biases related to the sequencing platform and poor sample quality. Additionally HTS also suffers from inherent difficulties such as PCR-amplification bias and uneven fragment distribution \cite{ross2013characterizing}.Therefore, quality control is one of the requirements during the analysis procedure. 

Several tools are available to check the quality of a sequencing experiment.The most widely used tool applied for the initial quality check is FastQC (http://www. bioinformatics.babraham.ac.uk/projects/fastqc). It analyzes sequencing data and provides certain statistics, including per-base sequence quality, GC-content, read length distribution, non-detected nucleotide quantity, duplication level and adapter types. Each statistics value allows detection of certain problems in the sequencing experiment. Moreover, warnings are reported based on default expected values.

It is worth noting that certain problems, such as coverage bias or issues with insert size of paired-end reads can be detected only after further RNA-seq analysis steps are performed. Therefore, there are special quality control tools focused on processing of the results of the subsequent analysis operations. For example, we developed one such tool called Qualimap \cite{garcia2012qualimap}, that analyzes alignment data in BAM format and characterizes its quality. 

Importantly, there are technical and algorithmic errors specific only to RNA-seq analysis. Despite the smaller size of the transcriptome compared to the genome, RNA-seq data analysis is challenged by the existence of complex regulatory mechanisms like alternative splicing, transcription of processed pseudogenes, dynamic concentration range of isoform expression, etc. Thus, additional rigorous quality control measures are required for processing of RNA-seq data. There are issues, such as 5'-3' bias or over-expression level of a distinct RNA type, which occur only in RNA-seq data. The detailed quality control of RNA-seq will be described carefully in the next chapter. Here, we will continue with the following data processing steps.

\subsection{Primary analysis}

There are two typical analysis types of sequencing reads:

\begin{itemize}
	
\item{Alignment} 

Most of RNA-seq experiments are performed on organisms whose genomes or even transcriptomes are already known; therefore, alignment of reads can be applied to perform the analysis. Basically, alignment is a computational operation to detect the position where the read is mapped to the reference sequence. However alignment to the transcriptome can be more complicated. Since there are exons and introns in higher eukaryotes, there are additional difficulties with accomplishing the RNA-seq data alignment. 

There are two ways to perform this task:

\begin{enumerate}

 \item Alignment is performed to already known transcriptome sequences available in databases such as Ensembl. This method is similar to whole genome sequencing data alignment and there is a number of effective tools available for this task such as Bowtie \cite{langmead2009ultrafast} and BWA \cite{li2009fast}. 

 \item Alignment is performed directly to the genome, taking into account reads breaking within introns. In this case an additional algorithmic approach is required to align reads that cover exon breaks. To detect these events, reads are separated into small segments that are used to reconstruct the isoform structure. The most frequently applied  tools for this task are currently Tophat \cite{trapnell2009tophat}, GSNAP \cite{wu2010fast} and STAR \cite{dobin2013star}.

\end{enumerate}

It is worth mentioning that results of alignment procedure are provided in a standard file format called Sequence Alignment/Map Specification (SAM). This format keeps the read alignment positions, along with specific properties such as pair type, mutations, insertions, deletions, duplications, etc. Additionally, data in SAM format can be easily packed to binary archive (BAM), which allows a significant reduce in the file size. 

%TODO: add figure with read alignment to genome?

\item{Assembly} 

The alignment process can be performed only if the reference sequence is available. However, in certain organisms this is not possible due to lack of existing information and the sequences must be reconstructed from reads. Popular tools to apply this technique are Trinity \cite{grabherr2011full} and Oases \cite{schulz2012oases}. 

\end{itemize}

Additionally, the exon-intron model highlights the importance of a detailed reconstruction of isoforms. In this case assembly can be combined with alignment to verify the isoform evidence. The widely used approaches that combine these techniques to detect isoforms are described further. 


\subsection{Gene expression analysis} 

After the alignment or assembly of reads is performed, it is possible to detect the expression of a particular gene based on the computation of \textit{coverage} - the number of aligned reads covering a certain fragment.

It is assumed that if the number of reads mapping to a certain biological feature of interest, such as a gene or a transcript, is sufficient, then it can be used as an estimation of the abundance of that feature in the sample and interpreted as the quantification of the expression level of the corresponding region. To apply this approach the number of reads aligned to a certain gene is counted to measure expression level. There are several methods based on this technology such as edgeR \cite{robinson2010edger} or DESeq \cite{anders2010differential}. 
 
Additionally, a more advanced type of analysis performs assembly of reads into full-length transcripts, and coverage estimation is based on the number of reads covering the breakpoints between exons and accurate distribution of reads located in exons. One of the most popular tools based on this approach is Cufflinks \cite{trapnell2010transcript}.

An important step in computation of gene expression level is the consideration of the impact of noise and the systematic variation between samples. Therefore there are specific normalization techniques available.  The most commonly used method normalizes the counts for exon length, assuming that read count distribution is the same in all samples \cite{mortazavi2008mapping}. The computed expression computation is reported in Reads Per Kilobase of exon per Million reads sequenced (RPKM) calculated in the following way: \[ RPKM = \frac{R * 10^9}{L * M}  \] In this function \textit{R} - reads mapped to the gene, \textit{L} - length of the gene, \textit{M} - the total number of mapped reads in the experiment.  There is also a variation of RPKM value that takes into account paired-end reads called Fragments Per Kilobase of exon per Million mapped fragments \cite{trapnell2010transcript}. It is worth noting that the RPKM value might introduce problems, since certain genes have very high expression values, and influence expression comparison results. To fix this issue additional methods have been developed to improve the normalization, such as upper quartile scaling \cite{bullard2010evaluation} or trimmed mean of M values\cite{robinson2010scaling}. Additionally, there could be a number of non-specific transformations that influence the results, including genetic background, time point and cell type heterogeneity. One such novel method was introduced to validate a variety of biological conditions \cite{risso2014normalization} and demonstrated appropriate results in comparison to other normalization methods \cite{peixoto2015data}.

%TODO: more references

%\subsection{Comparison of different biological conditions}
One of the main points of RNA-seq data analysis is the comparison between biological conditions. Detailed observation of gene expression difference is a complicated task due to several reasons: number of genes and samples, statistical significance of the event, systematic variation noise. The normalization, described previously, partly solves the noise problem, however additional estimations should be performed to validate the correctness of the results. Detection of differentially expressed genes is performed by comparing the expression thresholds of samples in different conditions by taking into account statistical modulations such as \textit{p-values}. The toolkits performing computation of gene expression including Cufflinks and DEseq also enable detection of differentially expressed genes, applying various statistical verifications. 

Notably, the best practice for gene expression analysis has not been defined yet. Due to complicated sequencing issues such as GC-content, paired-end read size and gene body coverage structure, current tools still have limitations in correct detection of differential gene expression \cite{rapaport2013comprehensive} and more novel methods are being developed to improve the status ( for example, Cuffdiff2 \cite{trapnell2013differential}, DESeq2 \cite{love2014moderated} and PennSeq \cite{hu2014pennseq}). Interestingly, a recent detailed comparison of current gene expression analysis methods based on simulation data demonstrated that inaccurate results are computed for hundreds of genes in \textit{Homo Sapiens} transcriptome even by most popular and effective tools \cite{robert2015errors}. This investigation confirms that gene expression determination from RNA-seq data requires further specific improvements.


\section{Additional aspects of RNA-seq data analysis}

\subsection{Isoform detection}

A lot of genes in higher eukariotic organisms have numerous splice variants, promoters and protein products. Understanding of the isoform structures allows correct measurement of changes in expression of individual transcripts and their influence on cellular processes.

However, detection of isoforms is complex, because in most genes alternative isoforms share large amounts of sequences and differences in isoforms rely on exon extraction or intron region inclusion. Moreover, the read coverage distribution inside transcripts is biased due to polyA selection or rRNA depletion \cite{lahens2014ivt}. Additionally, a lot of genes have high sequence similarity and as a result sequencing reads align to a number of genes (so called \textit{multi-mapped reads}). Therefore, counts for gene transcripts must be estimated carefully. 

Early computational methods to detect isoform expression performed statistical normalization for isoform-level \cite{katz2010analysis}. Later developed methods provided accurate reconstruction of isoforms followed by statistical representation of the coverage \cite{trapnell2012differential,mezlini2013ireckon} or focused only on exon blocks that perform the construction of various isoforms \cite{anders2012detecting}. Interestingly, different existing methods can report completely different results for the same dataset. A recent study compared existing tools for isoform detection on simulated and real datasets \cite{hayer2014benchmark}. The research demonstrated high error rate for certain tools along with lower recall and precision  specific to analysis of real data, concluding that advancement of algorithmic approaches and technology improvements should continue. 

\subsection{Strand-specificity}

One of the interesting aspects of transcriptome activity is that certain genes might have both sense and anti-sense transcription. In several situations anti-sense transcription of a gene reduces its own expression level or controls translation \cite{li2013integrated}. Standard RNA-seq libraries based on RNA to cDNA conversion did not support strand-specificity detection, because during sequencing process the synthesis of double-stranded cDNA was followed by a random addition of adapters to 5' and 3' ends. 

Of course, was possible to identify the strand of a certain gene using algorithmic approaches such as open reading frame information, biases in coverage of 5' and 3' ends or splice orientation. However, the technical validation of the strand would help to discover antisense transcripts, distinguish the strand of non-coding RNAs and correctly resolve expression levels of intersecting genes in different strands. 

%TODO: add figure here, showing strand specificity, there is a nice blog: http://onetipperday.blogspot.de/2012/07/how-to-tell-which-library-type-to-use.html

Therefore certain methods were developed to control strand-specificity during sequencing process \cite{levin2010comprehensive}. The techniques are based on attaching  different adapters to 5' and 3' ends (for example, Illumina RNA, NSR or SMART-RNA ligation) or on marking one strand by chemical modification (most common is dUTP ). All existing methods are integrated into a standard sequencing procedure. Even though the techniques also introduce specific errors, in general such approaches lead to novel detection of antisense transcripts along with detailed specification of intersecting genes and non-coding RNAs belonging to regions of known mRNAs \cite{core2008nascent}.


\subsection{Gene fusion detection}


DNA damage events such as mutations, insertions, deletions and others might have dangerous consequences. The most well-known result is the generation of mutated cells that lead to cancer. Quick and correct detection of these events in cells will allow design of medical approaches to disable cancer progression. 

One well-known example of such an event is a fusion gene detected in chronic myeloid leukemia (CML). A fusion gene is a combination of blocks of genes due to a break in the genome, such as insertion, deletion or translocation. Transcription and translation of a fusion gene can lead to the creation of a protein with dangerous unexpected activity. Detection of events such as the BCR-ABL fusion in CML led to the development of a treatment for this type of cancer by blocking the novel protein \cite{lugo1990tyrosine}.  

Studying gene fusions in the transcriptome enables detection of the rearrangements that might be translated into novel functional proteins. One of the most promising approaches is the detection of fusion genes from RNA sequencing data. It was shown that the detection of fusions from RNA-seq data is more convenient than from genome sequencing. For example, based on RNA-seq data analysis around 8000 fusions were detected in 4000 samples from various cancers, leading to potential medical aspects \cite{yoshihara2014landscape}. However, the methodology of fusion detection from transcriptome sequencing data is rather complex due to the homology of the genome and sequencing technology limitations. Some aspects of the topic, such as precision and recall properties of available methods, still require additional research. The third chapter of this thesis describes the current status in detail.

\section{Research goals}

RNA-sequencing analysis enables detailed detection of gene expression in a cell. In this aspect it demonstrates higher quality and confidence in comparison to other methods (i.e microarray analysis). Additionally, RNA-sequencing provides a number of important novel analysis approaches to investigate the transcriptome activity. 

Certainly, the main goals of RNA-seq data analysis already have quite acceptable solutions. However, the existing methods are not completely correct and there is a number of different approaches to reach improved solutions for such problems as transcriptome assembly, gene expression analysis, isoform and strand-specificity detection.
 
Moreover, certain aspects of RNA-seq still require novel resolutions. For example, the RNA-seq process can introduce a number of characteristic biases and problems, that can lead to incorrect analysis results. There is a number of elements that should be taken into account before the results of an experiment can be trusted. Therefore, detailed quality control of RNA-seq data is an important task. Chapter 2 focuses on solutions of these problems and describes a novel tool, Qualimap2, which we developed to clarify this task. The manuscript characterizing this tool was published in the journal Bioinformatics \cite{okonechnikov2015qualimap}.

Additionally, the ability to detect certain dramatic events such as fusion genes is an important topic, since it will permit the application of RNA-seq not only in scientific research, but also in clinical care. However, due to the complex structure of the transcriptome in higher eukaryotic organisms, detection of fusions is a complicated task. Moreover, certain aspects, such as for example strand-specificity, are currently not yet taken into account during the fusion detection. Chapter 3 describes a novel toolkit for fusion detection from RNA-seq data called InFusion, which we developed to solve existing problems and advance abilities of transcriptome sequencing application. The paper describing InFusion was submitted to PLOS Computational Biology.

Importantly, RNA sequencing technology continues to improve. Specific aspects, such as increase of read size and RNA processing without cDNA transformation provide appreciable improvements. Fruitful novel techniques such as single cell RNA-sequencing grant a lot of opportunities, but require redesigned analysis approaches. Moreover, the increase in number of samples and experiments in the analysis present additional statistics demands. Chapter 4 provides a discussion about the novelties and focuses on future research targets.




