\chapter{Quality control of RNA-seq alignment and expression data } 

\section{Overview}

This chapter characterizes the quality control requirements for correct processing of RNA-seq data. It starts with a description of existing technical problems and outlines possible ways to detect and validate them. Further, the design and functionality of a novel approach to accomplish RNA-seq quality control are described and comparison to other existing tools is performed. Finally, future research plans in this area are presented.   

\section{Reasons and approaches} 


RNA-seq is a powerful technology that provides a full determination of the transcriptome functionality. However, due to a complex technological structure of high-throughput sequencing, it includes a number of  biases that infiltrate the analysis process.  Moreover, cDNA synthesis, which is currently the most frequent RNA-seq approach root, introduces a number of specific errors, occurring only in RNA sequencing \cite{hansen2010biases}. 

There is a number of additional sources of a bias. First of all, RNA-seq demonstrates a non-uniform distribution of read coverage in a transcript because of particular errors occurring during RNA extraction \cite{kim2012short}, rRNA depletion \cite{he2010validation}, adapter ligation \cite{faulhammer2000fidelity} and PCR amplification \cite{kozarewa2009amplification}. Second, both read length and insert size of sequencing fragments limit the detection of a whole scope of  the transcriptome \cite{oshlack2009transcript}. Third, read mapping and assembly operations might introduce certain limitations and biases due to read errors and algorithmic problems \cite{li2010rna}. Also, the normalization of gene expression levels based on the processing of homologous reads leads to incorrect expression profiling.    

Additionally, during the process of reverse transcription, the generated cDNA can dissociate from the correct RNA sequences and connect to a different RNA. This event leads to the generation of chimeric transcripts, that do not exist in reality. Also, the generated second strand of cDNA blocks the detection of strand-specificity of the transcript. Strand-specific libraries that solve this problem can also have some limitations, influencing the computed anti-sense transcription level results \cite{levin2010comprehensive}.

Finally, experiments applying RNA sequencing can include a number of samples in different conditions. Even though the technology has the highest level of reproducibility in comparison to microarray approach, the analysis of genes with low expression level is limited and requires replicates. Unfortunately, a large number of samples might provide additional problems when a subgroup of outliers changes the total results of the analysis. 

The detection of occurring errors and mistakes is quite important to be sure that the results of the analysis are correct. Therefore, specialized tools are required to perform detailed quality control (QC). A number of problems can be detected by general approaches for analysis of HTS data (for example, FastQC tool), however problems, specific only for RNA-seq data, need additional analysis operations for the generated datasets such as alignment of reads or gene expression counts.

%TODO: reference manuscript about error rate: http://www.genomebiology.com/2015/16/1/177
% Intersting point: For hundreds of genes in the human genome, RNA-Seq is unable to measure expression accurately. These genes are enriched for gene families, and many of them have been implicated in human disease. We show that it is possible to use data that may otherwise have been discarded to measure group-level expression, and that such data contains biologically relevant information.

To solve this task several tools were developed. One of the first approaches was RSeQC tool \cite{wang2012rseqc}, which is actually a list of Python scripts that provide statistics computed from the alignment data. The performed analysis results include such aspects as accurate properties of mapped reads, insert size distribution for paired-end reads, gene body coverage, proportion of reads mapping to gene structure (5' UTR, coding, 3' UTR), estimation of sequencing depth in RPKM and junction saturation. The results for each analysis type are generated separately and each script has various options and requirements. 

Other similar method is RNA-seq QC \cite{deluca2012rna}, a functional pipeline with a serial number of steps. Except of QC control of BAM files it also produces read counts and performs additional QC operations, including detailed coverage analysis, detection of rRNA reads, expression profile efficiency examination and strand-specificity validation. Moreover, a limited coverage correlation analysis can be performed for a number of samples.  

We also developed Qualimap \cite{garcia2012qualimap} tool, that allowed precise general quality control analysis of BAM files along with counts data. Qualimap was able to detect a number of problems particular to any type of a sequencing experiment including whole-genome sequencing, exome sequencing and RNA-sequencing, while the counts QC mode was providing a solution for detection of QC issues related only to gene expression analysis. 

However, additional biases occurring only in RNA-seq experiments were not supported by Qualimap. Also, comprehensive RNA-seq alignment statistics, such as for example, exon coverage proportion and strand-specificity validation, were not available. Moreover, a frequently applied multi-sample RNA-seq analysis can be biased by outliers, thus their detection should be verified in detail. Finally, the performance of the existing tools was not compared so far.



\section{Qualimap2: advanced RNA-seq and counts quality control}

\subsection{Tool description}

The second version of Qualimap tool was developed to handle the described limitations. One of the most important reasons to update the tool was a list of QC requirements specific only to RNA-seq data analysis, which were not supported by the first version. Therefore, Qualimap2 includes a novel mode focused on RNA-seq alignment data quality control. Moreover, the read counts QC mode, introduced in the first version, was redesigned to support global analysis of multiple samples and comparison of sample groups.

In general Qualimap2 is a multiplatform user-friendly application with both graphical user (GUI) and command line interfaces. It includes four analysis modes: \textbf{BAM QC}, \textbf{Counts QC}, \textbf{RNA-seq QC} and \textbf{Multi-sample BAM QC}. The latter two modes are first introduced in the second version. 

Based on the selected type of the analysis, users provide input data in the form of a BAM/SAM alignment, GTF/GFF/BED annotation and/or read counts table. The results of the QC analysis are presented as an interactive report from GUI, as a static report in HTML or PDF format and as a plain text file suitable for parsing and further processing. Typically, the report contains summary statistics of the dataset, description of the input data, exploratory plots and histograms that visualize multiple properties of the processed data and help to detect potential problems. 

The mode \textbf{BAM QC} is an initial mode that performs detailed analysis of the alignment data in SAM/BAM format. As it was mentioned previously, it allows detection of a number of problematic issues related to any type of a sequencing experiment. For example, it provides extensive alignment statistics along with coverage plots and histograms. Importantly, certain aspects such as number of marked mutations, insertions, deletions, insert size qualities and other properties that can be detected only from the alignment data are reported.  %TODO More info about BAM QC

\textbf{Multiple BAM QC} mode is a novel mode, which is also focused on analysis of BAM files in general. It takes into account and combines results created by \textbf{BAM QC} mode to create plots combining statistics from a number of samples. Moreover, principal component analysis (PCA) is applied to detect outliers based on the selected statistics. 

The two modes designed for RNA sequencing data QC analysis are further described in detail.

\subsection{RNA-seq QC mode}

A novel mode \textbf{RNA-seq QC} reports quality control metrics and bias estimations specific only for the whole transcriptome sequencing, including reads genomic origin, junction analysis, per-transcript coverage, consistency of library protocol and 5'-3' bias estimation. This mode can be applied as a complementary tool together with \textbf{BAM QC} mode.  
%TODO: expalin the algorithm?

The \textbf{RNA-seq QC} mode is designed as an algorithmic pipeline that analyzes alignments from an input BAM file. Each alignment is processed to collect defined statistics such as number of mapped reads, pair construction, etc. The transcriptome annotations are also required. Based on the annotation data, read alignments are analyzed to detect intersections with exon, intron or intergenic regions. During this operation analysis of the coverage of transcripts is performed; read counts and other important statistics are computed. 

After the analysis is finished the following results are reported:

\begin{itemize}

\item{Summary}

The summary contains several sections describing in detail statistics specific for RNA-seq alignment, including :

\begin{itemize}

\item{The assignment of read counts per-category}

The total number of mapped reads and the distribution of alignments belonging to a selected type are reported. The types include unique alignments, secondary alignments (duplicates marked by SAM flag), non-unique alignments (SAM format \textbf{NH} tag of a read is more than one), reads aligned to genes or without any feature (intronic and intergenic), ambiguous alignments and a number of unmapped reads.

\item{Transcript coverage profile}

The ratios between mean coverage at 5' region, 3' region and the whole transcript are reported. To compute this value for each transcript mean coverage along with mean coverage in the first 100 bp (5' region) and the last 100 bp (3'region) are calculated and collected. Afterwards, the collected values are sorted and median is selected from each array to compute the ratios.

\item{Reads genomic origin}

The report shows how many alignments fall into exonic, intronic and intergenic regions. Exonic region includes 5'UTR, protein coding region and 3'UTR region. To detect alignment positions, annotations data is used to generate all exonic and intronic intervals. A read alignment is checked if it intersects with an exon or an intron. In case intersection is not detected, it is considered as intergenic.

\item{Junction analysis} 

The total number of reads with splice junctions and 10 most frequent junction types are reported. The junctions are detected by analyzing SAM format CIGAR field. The \textit{N} operation detects the skipped region from the reference and represents read alignment covering an exon. Additionally, a pair of nucleotides from left and right side of a skipped region are analyzed to detect the junction rate. 

\end{itemize}

\begin{figure}[t] 
	\begin{center}
		\caption[RNA-seq QC plot examples]{RNA-seq QC plot examples. (A) Coverage profile of highly expressed genes (B) Coverage histogram (from 0 to 50X)}
		\label{fig:rnaseq_qc_example}
		\includegraphics[width=\textwidth]{figures/rseqc_sample.pdf} 
	\end{center}
\end{figure}

\item{Pie chart }

The plot shows how many read alignments fall into exonic, intronic and intergenic regions. Results computed and reported in the Summary are demonstrated in the plot.  

\item{Coverage Profile (Total)}

The plot shows mean coverage profile of the transcripts. All genes with non-zero coverage are used to generate this plot. From each gene only one transcript, having the highest expression, is selected.

\item{Coverage Profile (High)}

The plot shows mean coverage profile of 500 highest-expressed genes (Figure \ref{fig:rnaseq_qc_example}A). Transcript selection is similar to the total Coverage Profile.
	

\item{Coverage Profile (Low)}

The plot shows mean coverage profile of 500 lowest-expressed genes. Transcript selection is similar to the total Coverage Profile.

\item{Coverage Histogram}

The histogram demonstrates the coverage of transcripts from 0 to 50X (Figure \ref{fig:rnaseq_qc_example}B). The genes that have coverage higher than 50X are collected in the last column. 

\item{Junction Analysis}

The pie chart is focused on the types of junction positions in spliced alignments. \textit{Known} category represents percentage of alignments where both junction sides are known from the annotation. \textit{Partly known} value represents alignments where only one junction side is known. All other alignments with unknown junctions are marked as \textit{Novel}. 

\end{itemize}

Importantly, during the \textbf{RNA-seq QC} procedure the read counts are also computed. By default, a read alignment is considered supporting the gene and counted, only if it lies exactly inside of exon region. In case of paired-end reads, both pair mates should support the same gene or transcript. Counts computation includes a number of optional parameters. For example, the strand-specificity can be taken into account based on the selected protocol, i.e. if a mapped read doesn't fall the correct strand of a gene, it is not counted. Additionally, if a read is mapped to multiple locations, it is ignored by default. However, there is also an option to count a multi-aligned read as separated proportionally between targets. If this option is activated then, for example, a read mapped to 4 different locations will add 0.25 to the counts at each location. After the analysis is finished, the final counts value is converted to integer.

\subsection{Counts QC mode}

The counts data, generated during the \textbf{RNA-seq QC} procedure or computed using some other tool such as HTSeq \cite{anders2014htseq}, can be utilized to assess differential expression between two or more experimental conditions. However, before performing differential expression analysis, researchers should be aware of some potential limitations of RNA-seq data, as for example the saturation level influence on sequencing depth or feature types detected in the experiment. These and other properties can be analyzed by interpreting the plots generated by \textbf{Counts QC} mode.

In the second version of Qualimap \textbf{Counts QC} module has been redesigned to work with multiple samples under different conditions. The new functionality is mostly based on NOISeq package \cite{tarazona2012noiseq}, therefore to use \textbf{Counts QC} it is required to have R language along with certain packages installed.

To perform the analysis it is also necessary to provide a special table that contains information about input sample datasets. Each sample has a name and a condition, which describes the group. Therefore, biological differences can be easily mentioned by setting group conditions. Additionally, it is possible to perform not only a default analysis of all samples, but also compare the conditions.

\begin{figure}[t] 
	\begin{center}
		\caption[Counts QC global plot examples]{\textbf{Counts QC} Global Plot examples. (A) Saturation of expression (B) Scatterplot matrix }
		\label{fig:countsqc_global_sample}
		\includegraphics[width=1\textwidth]{figures/countsqc_global_sample.pdf} 
	\end{center}
\end{figure}

 
In result, after the processing of input data \textbf{Counts QC} mode generates three groups of plots.

\begin{enumerate}
	\item {\textit{Global Plots}}

	Plots from this group present a global overview of the counts data and include all samples. These plots allow to compare all samples without taking into account experimental conditions.
	
	\begin{itemize}
	\item{Counts Density}

	The plot shows density of counts computed from the histogram of log-transformed counts. In order to avoid infinite values in case of zero counts, the transformation \(log_2(expr + 0.5)\) is applied, where \textit{expr} is a number of read counts for a given feature. Only log-transformed counts having value greater than 1 are plotted.

	\item{Saturation}
	
	The plot provides information about the level of saturation in the samples and helps to decide if more sequencing is required (Figure \ref{fig:countsqc_global_sample}A). The sequencing depth of the sample is represented at the x-axis. Smaller depths correspond to samples randomly generated from the original sample.The curves are associated to the left y-axis represent the number of detected features at each of the sequencing depths in the x-axis. They show the number of newly detected features, when the sequencing depth increases in one million of reads.

	\item{Scatterplot Matrix}	

	The panel shows for each pair of samples a scatter plot along with a smoothed line in the lower panel and Pearson correlation coefficients in the upper panel (Figure \ref{fig:countsqc_global_sample}B). Plots are generated using log-transformed counts.


	\item{Counts Distribution}

	The boxplot shows the global distribution of counts in each sample. For each sample the mean value surrounded by quartiles is demonstrated. Additionally, detected outliers are marked.

	\item{Features} With Low Counts

	The plot shows the proportion of features with low counts in the samples. Such features are usually less reliable and could be filtered out. In this plot, the bars show the percentage of features within each sample having more than 0 counts per million (CPM), or more than 1, 2, 5 and 10 CPM. The detection of outliers is possible by comparison of CPM proportions.

\end{itemize}

\begin{figure}[t] 
	\begin{center}
		\caption[Counts QC individual sample plot examples]{\textbf{Counts QC} Individual Sample plots examples (A) Saturation with two y-axis demonstrating number of detected fusions along with number of novel detections per million of reads (B) Counts per biotype  }
		\label{fig:countsqc_single_sample}
		\includegraphics[width=1\textwidth]{figures/countsqc_single_sample.pdf} 
	\end{center}
\end{figure}

\item{\textit{Individual Sample Plots}}

Apart from the global overview, there are plots generated individually for each sample. When an annotation file  describing biotype, length and GC-content of each transcript is provided by the user, additional series of plots is generated.

\begin{itemize}
\item{Saturation}

For each sample, a saturation plot is generated like the one described in \textit{Global Saturation} (Figure \ref{fig:countsqc_single_sample}A). Additionally, the right side of the plot demonstrates the total number of detected features per million of reads.

\item{Bio Type Detection}

Since RNA-seq experiments might be designed to detect specific RNA types (i.e. microRNA or long non-coding RNAs) it is important to verify the count distribution across bio types. The barplot visualizes features that are detected in the sample. The x-axis shows all the groups provided in the annotations file such as for example protein coding, miRNA, lincRNA, pseudogene etc. The grey bars are the percentage of features of each group within the reference genome (or transcriptome, etc.). The striped color bars are the percentages of features of each group detected in the sample with regard to the genome. The solid color bars are the percentages that each group represents in the total detected features in the sample. 

\item{Counts Per Biotype}

The boxplot per each group describes the counts distribution in the given biotype (Figure \ref{fig:countsqc_single_sample}B). For each biotype the mean value surrounded by quartiles is demonstrated, additionally certain outliers are shown. The generated plot allows to compare the expression levels among biotypes and detect possible contamination.

\item{Length Bias}

The plot describes the relationship between the length of the features and the expression values. The length is divided into bins. Mean expression of features falling into a particular length interval is computed and plotted. A cubic spline regression model is fitted to explain the relation between length and expression. Coefficient of determination \(R^2\) and p-value are shown together with regression curve.

\item{GC Bias}

The plot describes the relationship between the GC-content of the features and the expression values. The data for the plot is generated similar to Length Bias plot. The GC content divided into beans and then mean expression features corresponding to given GC interval are computed. The relation between GC-content and expression is investigated using a cubic spline regression model.
\end{itemize}


\begin{figure}[t] 
	\begin{center}
		\caption[Counts QC comparison plot example]{\textbf{Counts QC} Comparison Plots example. Bio detection plot demonstrates the expression levels of various RNA types among selected groups. }
		\label{fig:countsqc_comparison_sample}
		\includegraphics[width=1\textwidth]{figures/countsqc_comparison_sample.pdf} 
	\end{center}
\end{figure}

\item{\textit{Comparison Plots}}

When an option to compare conditions is activated, additional plots comparing data in groups of samples having the same biological condition or treatment are generated. The samples belonging to a group are combined and mean values for each comparison are computed. Currently only two types of a group are supported.

\begin{itemize}
\item{Counts Distribution}

The plot is similar to the one in the \textit{Global Plots} report. It compares distributions of mean counts across conditions. 

\item{Features With Low Counts}

The plot is similar to the one in the \textit{Global Plots} report. It compares proportions of features with low counts by computing mean counts across conditions.

\item{Bio Detection}

The plot is similar to the one in the \textit{Individual Sample Plots} report (Figure \ref{fig:countsqc_comparison_sample}C). It compares distribution of the detected features for the given biotype by computing mean counts across conditions.

\item{Length Bias}

The plot is similar to the one in \textit{Individual Sample Plots} report. It analyzes relation between feature length and expression across conditions.

\item{GC Bias}

The plot is similar to the one in the \textit{Individual Sample Plots} report. It analyzes relation between GC-content and expression across conditions.
\end{itemize}


\end{enumerate}

All the described plots are designed to demonstrate biases or problems that can be detected only from the counts distribution. Moreover the \textit{Comparison Plots} group allows to compare two biological conditions. This is a frequent requirement in the transcriptome related research. 

It is worth noting that there are additional options that allow to control the counts QC analysis. For example, in order to remove the influence of spurious reads, counts threshold is applied to consider a transcript as detected only if its corresponding number of counts is greater than this threshold. By default, the threshold value is set to 5 counts.

\subsection{Insight from quality control analysis}

The statistics values computed from RNA-sequencing alignment and counts data allow to detect problematic issues that can not be discovered directly from the reads data. The most of the technological biases, including transcript length and coverage uneven distribution or contamination by non-required transcripts can be detected only from a processed dataset, therefore data investigation performed by Qualimap is required to confirm sufficient quality of the processed data. 

The quality control process should start from the examination of \textbf{RNA-seq QC} report. The first statistics of the mapped data provides the status of the sequencing process in general. The low proportion of aligned reads reported in the summary can demonstrate ligation and amplification biases. The typical majority of aligned reads should belong to known exonic regions. For example, comparing the proportion of aligned reads falling to exon region in case of well-known organisms such as \textit{Mus Musculus} or \textit{Homo Sapiens} should be up to 90\%. Smaller proportion can indicate sequencing biases or some mistakes in the mapping process. The same conclusions can be derived from \textit{"Junction analysis"} plot: known junctions should dominate novel. 
	
Some specific issues occurring during rRNA depletion and polyA selection can lead to biases in 5' region and 3' region. For example, it is quite well-known that polyA selection can lead to high expression in 3' region. The 5'-3' bias allows to detect such events. In correct experiments this bias should be close to one.

\begin{figure}[t] 
	\begin{center}
		\caption[Detection of biases from Counts QC report]{Detection of biases from \textbf{Counts QC} report (A) Global saturation demonstrates low coverage context of samples VCaP500 and LNCaP500 in comparison to samplse VCaP200 and LNCaP200 (B) The influence of the read length on expression: normalization is required  }
		\label{fig:countsqc_error_detection}
		\includegraphics[width=1\textwidth]{figures/countsqc_error_example.pdf} 
	\end{center}
\end{figure}


The plots focused on transcriptome coverage analysis such as "\textit{Coverage Histogram}" and "\textit{Coverage profile along genes}" give an overview of available level of expressed genes. Importantly, low level coverage in RNA-seq data influences significantly on differential gene expression analysis \cite{labaj2011characterization}, therefore demonstration of high and low coverage level is quite useful to detect biases and apply suitable gene expression normalization approach.

After the quality control of the BAM file is performed, the computed counts will allow to investigate further properties of the experiment. Most importantly, \textbf{Counts QC} analysis can be performed with taking into account the experimental design, such as biological conditions and the number of samples. 

The analysis of expression contamination allows to verify if the total number of reads in the experiment is enough to detect all expressed genes. The plot "\textit{Saturation}" demonstrates the influence of the number of sequencing reads on expression distribution. Basically, the angle of the line plot shows how the increase in number of reads controls the proportion of novel detected genes (Figure \ref{fig:countsqc_error_detection}A). If more reads do not lead to the growth of a number of detected genes (line angle is close to zero), additional sequencing is not required. Notably, there is also a specific customizable limitation for a minimum number of read counts required for a transcript to be added to the plot. 

The biotype analysis of counts performed for each sample allows to detect the types of expressed features. This is especially important if the RNA-seq experiment is focused on long RNA or other type of RNA. Abnormal contamination can be detected from the plots "\textit{Biotype detection}" and "\textit{Counts per biotype}". Typically, in mRNA-seq experiments protein coding proportion should dominate in counts. Additionally, the plots detect the suitability level of a selected sequencing protocol.	

For correct normalization of counts in gene expression analysis it is important to take into account the length and GC content of expressed transcripts. The requirements for such normalization can be checked from "\textit{Length bias}" and "\textit{GC bias}" plots (Figure \ref{fig:countsqc_error_detection}B). Cubic spline regression model is applied to detect if length and GC proportion fit the gene expression. Generally, the computed coefficient of determination greater than 70\% or a large \textit{p-value} indicates an effect on expression level and importance of normalization \cite{tarazona2015data}.

The computed global plots demonstrating all samples together such as scatterplot matrix, counts density and distribution allow to detect outliers. For example, despite different biological conditions the global expression levels among analyzed samples should match sufficiently. Importantly, different biological conditions should influence on gene expression. Therefore, all the plots related to expression analysis and normalized to a specific condition also allow to detect outliers.  

\subsection{Comparison to other tools}

Except of Qualimap there are other tools that are available to perform the quality control task. The most highly-used applications are RSeqQC and RNA-seq QC. We performed a detailed comparison of Qualimap \textbf{RNA-seq QC} and \textbf{Counts QC} modes to these methods. The results are provided in the table \ref{tab:rnaseq_qc_tools}.  It is worth noting that RSeQC and Qualimap also support general sequencing data analysis methods (including GC-content, number of mismatches and indels, insert size, mapping quality etc.). However, in this table these elements are not included, since it is focused on RNA-sequencing data analysis.

\bigskip
{
	\centering
	\begin{minipage}{\linewidth}
		\footnotesize
		\captionof{table}{Comparison of RNA-seq quality control tools.} \label{tab:rnaseq_qc_tools} 
		\begin{tabularx}{\textwidth}{    X |X| X| X   }\toprule[1.5pt]
			\bf Analysis type & \bf RSeQC & \bf RNA-SeQC & \bf Qualimap \\\midrule
			\hline
			Aligned reads statistics types & 	Non-splice  & 	Total, unique, duplicate and alternative; Vendor Failed Reads. & Total, secondary, aligned to genes, non-unique, no-feature and ambiguous \\
			\hline		
			Read pairs statistics & Pairs aligned, left/right &	Pairs aligned, unpaired reads; pair mismatch rate; chimeric pairs & Pairs aligned, left/right \\
			\hline
			Strand-specificty detection & Available & Available & Available \\
			\hline
			Alignments location analysis & Exonic (5'UTR, 3'UTR, CDS), intronic & Exonic (5'UTR, 3'UTR, CDS), intronic, TSS & Exonic, intronic, intergenic \\
			\hline
			Gene coverage analysis & Gene coverage over gene body plot & Coverage gaps (count, length); coverage plots & Coverage profile along genes (total, low, high); coverage histogram \\
			\hline
			5'- 3' bias analysis     & - & Available & Available \\
			\hline
			Gene expression computation& Available (RPKM)& Available (read counts, RPKM) & 		Available (read counts, RPKM) \\
			\hline
			Expression profiling & - & Efficiency (ratio of exon-derived reads to total reads sequenced); rRNA reads & Detailed percentage of expressed exon type; low counts detection \\
			\hline
			Multisample analysis & Coverage of several samples together & Correlation between each sample pair & Coverage density, scatterplot matrix, saturation, counts distribution \\
			\hline
			Group comparison & - & - & Counts distribution, bio detection, length bias, GC bias \\
			\hline
				
			\bottomrule[1.25pt]
			\end {tabularx}\par
			\bigskip
			\small
			\textit{Comparison of existing RNA-seq quality control tools: RSeQC v2.6, RNA-SeQC v1.1.8 and Qualimap v2.1. }
		\end{minipage}
	}\bigskip

According to this table, quality control tools have some similar functions. However, as it can be seen, Qualimap outperforms other programs in multi-sample analysis and group comparison blocks, providing a number of plots that allow to compare samples and detect outliers. The novelties of Qualimap are mostly focused on comparison of various conditions.

Moreover, Qualimap2 demonstrated superior performance in comparison to other tools. It showed twice work speed increase in typical \textit{H. sapiens} RNA-seq data analysis. Moreover, the analysis performed by RSeQC requires to launch and control a number of different scripts, thus additional work is necessary to design an appropriate pipeline.

\subsection{Results and future plans}

Qualimap2 application performs an important task: detection of errors and limitations specific to RNA sequencing. Overall, the second version of Qualimap was downloaded more than 4000 times since the initial release in September 2014. Importantly, some of the detected QC biases can be fixed during analysis applying normalization techniques, however certain issues can not be improved, therefore novel experiments might be required. There are already examples of publications where  Qualimap reports were contributing to relaunch of sequencing experiments \cite{koeppel2015helicobacter}. 

One interesting subject to mention: Qualimap2 is an open-source tool, which has a public repository. After the second version was released, a number of suggestions to improve required aspects of quality control came from user community. Additionally, specific bugs were reported and fixed by users. 

However, some elements of RNA-seq data analysis should be interpreted in more detail. For example, influence of read size and insert size can be processed to verify specific RNA-seq  limitations for gene expression analysis. Novel companies such as PacBio (www.pacificbiosciences.com) provide rather long read size resulting in a number of specific errors, which detection is quite important. Recently created single cell RNA-seq process introduces even more biases and issues that influence the analysis results \cite{stegle2015computational}. Moreover, currently experiments might include a number of different conditions, while Qualimap2 supports in \textit{Comparison} mode only two conditions. Therefore, adaption of Qualimap should continue in these directions.

\section{Summary}

As it was described in previous sections, RNA-sequencing might provide various technical biases and errors that can influence the results of the analysis. To detect these events we developed the second version of Qualimap, an application for exploratory analysis and quality control of HTS alignment data written in Java and R. Qualimap2 introduces a novel analysis mode called \textbf{RNA-seq QC}. This mode allows computation of metrics specific for RNA-seq data, including per-transcript coverage, junction sequence distribution and reads genomic localization. Furthermore RNA-seq QC estimates 5'-3' bias and consistency of the library protocol. 

The mode \textbf{Counts QC}, created in the first version to estimate the quality of the read counts, was completely redesigned to allow processing of multiple samples. Having multiple biological replicates per condition is quite common in RNA-seq experiments; therefore it is beneficial to be able to analyze counts data from all generated datasets simultaneously. Multi-sample analysis allows inspection of grouping of the samples, as well as discovery of outliers and batch effects. Similar to the previous version, the \textbf{Counts QC} mode estimates the saturation of sequencing depth, counts density, correlation of samples and distribution of counts among classes of selected features. Additionally there are new plots that explore the relationship between expression values and GC-content or transcript length are available for users.  The analysis results include a combined overview of the datasets along with a QC report for each individual sample. Moreover the analyzed samples can include two different conditions, e.g. treated and untreated. In this case, separate plots comparing groups of samples corresponding to a particular condition are generated.   

Overall, Qualimap2 has become an important tool for quality control of RNA-seq experiments. The number of downloads and citations of the initial manuscript increased significantly after the release of the second version. It is worth noting that Qualimap has gathered a community of users who frequently report existing problems, suggest new features and contribute their code. 

